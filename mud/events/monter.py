# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class MonterEvent(Event2):
    NAME = "monter"

    def perform(self):
        if not self.object.has_prop("montable") or self.actor.has_prop("sur_objet"):
            self.fail()
            return self.inform("monter.failed")
        self.actor.add_prop("sur_objet")
        self.inform("monter")
