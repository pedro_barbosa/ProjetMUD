from .effect import Effect3
from mud.events import ShowEvent

class ShowEffect(Effect3):
    EVENT = ShowEvent
