from .action import Action3
from mud.events import ShowEvent

class ShowAction(Action3):
    EVENT = ShowEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "show"
