# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import HitEvent, HitWithEvent

class HitAction(Action2):
    EVENT = HitEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "hit"

class HitWithAction(Action3):
    EVENT = HitWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "hitWith"
